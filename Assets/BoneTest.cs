﻿using UnityEngine;
using System.Collections;

public class BoneTest : MonoBehaviour {
	public Material mat;
	public Transform[] bones;

	// Use this for initialization
	void Start () {
		var poses = new Matrix4x4[4];
		for (var i = 0; i < poses.Length; i++)
			poses[i] = bones[i].worldToLocalMatrix * transform.localToWorldMatrix;

		var weights = new BoneWeight[4];
		weights[0].boneIndex0 = 0;
		weights[0].weight0 = 1;
		weights[1].boneIndex0 = 1;
		weights[1].weight0 = 1;
		weights[2].boneIndex0 = 2;
		weights[2].weight0 = 1;
		weights[3].boneIndex0 = 3;
		weights[3].weight0 = 1;

		var mesh = new Mesh();
		mesh.vertices = new Vector3[]{ 
			new Vector3(-0.5f, -0.5f, 0f), new Vector3(0.5f, -0.5f, 0f), new Vector3(-0.5f, 0.5f, 0f), new Vector3(0.5f, 0.5f, 0f) };
		mesh.triangles = new int[]{ 0, 3, 1, 0, 2, 3 };
		mesh.boneWeights = weights;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		mesh.boneWeights = weights;
		mesh.bindposes = poses;

		var skin = gameObject.AddComponent<SkinnedMeshRenderer>();
		skin.sharedMaterial = mat;
		skin.bones = bones;
		skin.sharedMesh = mesh;
	}
}
