﻿using UnityEngine;
using System.Collections;

public class BoneGrip : MonoBehaviour {

	void OnMouseDrag() {
		var camera = Camera.main;
		var screenPos = Input.mousePosition;
		screenPos.z = Vector3.Dot(camera.transform.forward, (transform.position - camera.transform.position));
		transform.position = camera.ScreenToWorldPoint(screenPos);
	}
}
